var Connection = require('tedious').Connection;

var config = {
    server: 'SIM-ST19-29023',
    authentication: {
        type: 'default',
        options: {
            userName: 'sa',
            password: 'HMLVsa2013'
        }
    },
    options: {
        database: 'MTS',
        instanceName: 'MSSQLSERVER01',
        rowCollectionOnDone: true,
        useColumnNames: false
    }
}

var connection = new Connection(config);

connection.on('connect', function(err) {
    if (err) {
        console.log(err);
    } else {
        console.log('Connected');
    }
});

module.exports = connection;