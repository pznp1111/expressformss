var express = require('express');
var bodyParser = require('body-parser');
const bearerToken = require('express-bearer-token');
const clientSessions = require('client-sessions');


var app = express();

var port = process.env.port || 3300

app.listen(port, () => {
    console.log("Hi This port is running");
});

// token reader
app.use(bearerToken());

// parse setting
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// client clientSessions
app.use(clientSessions({
    cookieName: 'sessions', // cookie name, and use req.sessions to set value
    secret: 'howardisgood',
    duration: 5 * 60 * 1000, // how long the session will stay valid in ms
    cookie: {
        path: '/api', // cookie will only be sent to requests under '/api'
        maxAge: 5 * 60 * 1000,
        httpOnly: false, // when true, cookie is not accessible from javascript
        ephemeral: false // when true, cookie expires when the browser closes
    }
}))

var router = require('./routes')();

app.use('/api', router);

// app.listen(3000, (err) => {
//     console.log('Server listen on port 3000...');
// });