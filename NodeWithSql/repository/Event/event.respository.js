var response = require('../../shared/response');

var TYPES = require('tedious').TYPES;
var jwt = require('jsonwebtoken');

var constant = require('../../constant');

function EventRepository(dbContext) {


    function startEvent(req, res) {

        var par = JSON.parse(JSON.stringify(req.body));

        //  var par = jwt.decode(req.token);
        //   console.log("decoded", decoded);
        console.log("************startEvent************");
        console.log("par", par);
        //need to do properly
        var userInfo;
        if (req.token != undefined) {
            userInfo = jwt.decode(req.token);
            console.log("user", userInfo);
        } else {
            return res.sendStatus(401).send('missing token');
        }

        console.log("par", par);

        if (par.TaskName != undefined && par.StartTime != undefined && par.EndTime != undefined) {
            console.log("test");
            var parameters = [];
            parameters.push({ name: 'TaskName', type: TYPES.NVarChar, val: par.TaskName });
            // parameters.push({ name: 'StartDate', type: TYPES.Date, val: par.StartDate });
            // parameters.push({ name: 'EndDate', type: TYPES.Date, val: par.EndDate });
            console.log("parameters", parameters);
            var query = "INSERT INTO EventDetail (TaskId,TaskName,StartTime,EndTime,SupervisorId,LocationPostalCode) " +
                " select * from (select distinct a.TaskId, a.TaskName, " +
                " CONVERT(DATETIME, CONVERT(CHAR(8), a.FromDate, 112) + ' ' + CONVERT(CHAR(8), a.FromTime, 108)) as StartTime, " +
                " CONVERT(DATETIME, CONVERT(CHAR(8), a.ToDate, 112) + ' ' + CONVERT(CHAR(8), a.ToTime, 108)) as EndTime " +
                " ,a.EmployeeId as SupervisorId ,c.PostalCode as LocationPostalCode " +
                " from " + constant.planning + "dbo.View_MSS_GetSkills a " +
                " inner join " + constant.planning + "[ManpowerScheduling].[Location] c on a.LocationId = c.id " +
                " where a.TaskName = @TaskName " +
                " and CONVERT(DATETIME, CONVERT(CHAR(8), a.FromDate, 112) + ' ' + CONVERT(CHAR(8), a.FromTime, 108)) = '" + par.StartTime + "' " +
                " ) as tmp" +
                " where not exists ( select  TaskId,TaskName ,StartTime,EndTime,SupervisorId,LocationPostalCode from EventDetail where TaskName = @TaskName " +
                " and StartTime = '" + par.StartTime + "')  ;" +
                // the above query insert to EventDetail if no record exists

                " insert into ManpowerDetail(EmployeeId,DisplayName,EventId,Skill)" +
                " select * from (select a.EmployeeId,a.DisplayName,(select EventID from EventDetail where TaskName = @TaskName and StartTime = " +
                " '" + par.StartTime + "'  ) as EventID,a.Skill " +
                " from " + constant.planning + "dbo.View_MSS_GetSkills a " +
                " where a.TaskName = @TaskName " +
                " and CONVERT(DATETIME, CONVERT(CHAR(8), a.FromDate, 112) + ' ' + CONVERT(CHAR(8), a.FromTime, 108)) = '" + par.StartTime + "' " +
                " ) as tmp " +
                " where not exists(select EmployeeID,DisplayName,EventId,Skill from ManpowerDetail where EventID = (select EventID from EventDetail where TaskName = @TaskName and StartTime = " +
                " '" + par.StartTime + "'  ) and EmployeeID = " + userInfo.EmployeeId + " );" +
                //the above query insert tp ManpowerDetail if no record exists

                " select e.TaskId,e.TaskName,e.StartTime,e.EndTime,e.SupervisorId,e.LocationPostalCode , " +
                " (select * from ManpowerDetail where EventId = e.EventId FOR JSON PATH) as ManpowerDetail " +
                " from EventDetail e " +
                " where e.TaskName = @TaskName " +
                " and e.StartTime = '" + par.StartTime + "'";
            //  return result that just inserted
            console.log("query", query);
            dbContext.getQuery(query, parameters, false, function(error, data) {
                console.log("data0", data);
                console.log("error", error);
                if (data) {
                    console.log('result1', data);
                    return res.json(response("completed", data));
                } else {
                    return res.status(200).send('');
                }
            });

        } else {
            return res.sendStatus(401);
        }

    }

    function dispatchParticipation(req, res) {


        var par = JSON.parse(JSON.stringify(req.body));

        //  var par = jwt.decode(req.token);
        //   console.log("decoded", decoded);
        // console.log("par.LoginID", par.LoginID);
        //need to do properly
        console.log("*****************dispatchParticipation*****************", par);
        var userInfo;

        if (req.token != undefined) {
            userInfo = jwt.decode(req.token);
        } else {
            return res.sendStatus(401).send('missing token');
        }
        console.log("userInfo", userInfo);
        //  var planningDB = "MSS_PSA.";
        //  if (par.TaskName != undefined && par.FromDate != undefined && par.FromTime != undefined && par.ParticipantEmployeeID) {
        if (par.TaskName != undefined && par.StartTime != undefined && par.EndTime != undefined && par.ParticipantEmployeeID) {
            var parameters = [];
            var d = new Date();
            var n = (d.getTimezoneOffset() / 60) * (-1); //to fix timezone difference
            var timeInMss = new Date().addHours(n).toISOString().slice(0, 19).replace('T', ' ');
            console.log("timeInMss", timeInMss);
            parameters.push({ name: 'TaskName', type: TYPES.NVarChar, val: par.TaskName });
            parameters.push({ name: 'StartTime', type: TYPES.Date, val: par.StartTime });
            parameters.push({ name: 'EndTime', type: TYPES.Date, val: par.EndTime });
            //  parameters.push({ name: 'ParticipantEmployeeID', type: TYPES.NVarChar, val: par.ParticipantEmployeeID });
            // parameters.push({ name: 'CurrentTime', type: TYPES.DateTime, val: timeInMss });
            parameters.push({ name: 'Action', type: TYPES.NVarChar, val: 'Start' });
            parameters.push({ name: 'SenderId', type: TYPES.NVarChar, val: userInfo.EmployeeId });

            var ParticipationQuery = "insert into ActionParticipation(ManpowerID,Action,Time,SenderId) " +
                " select (select m.ManpowerID from ManpowerDetail m  " +
                "inner join EventDetail e on m.EventId = e.EventId " +
                " where m.EmployeeId = @ParticipantEmployeeID " +
                " and e.TaskName = @TaskName " +
                " and e.StartTime = '" + par.StartTime + "'  " +
                " and e.EndTime = '" + par.EndTime + "'  " +
                " ) as ManpowerID, @Action,'" + timeInMss + "'," + userInfo.EmployeeId + "";
            //check if there is no record in the Participation Table
            ParticipationQuery = ParticipationQuery +
                " WHERE not exists ( select * from ActionParticipation where ManpowerId = (" +
                " select m.ManpowerID from ManpowerDetail m  " +
                "inner join EventDetail e on m.EventId = e.EventId " +
                " where m.EmployeeID = @ParticipantEmployeeID " +
                " and e.TaskName = @TaskName " +
                " and e.StartTime = '" + par.StartTime + "'  " +
                " and e.EndTime = '" + par.EndTime + "'  " +
                " ) );" +
                " select * from ActionParticipation where ManpowerID  = " +
                " (select m.ManpowerID from ManpowerDetail m  " +
                "inner join EventDetail e on m.EventId = e.EventId " +
                " where m.EmployeeID = @ParticipantEmployeeID " +
                " and e.TaskName = @TaskName " +
                " and e.StartTime = '" + par.StartTime + "'  " +
                " and e.EndTime = '" + par.EndTime + "'  " +
                " )"
            console.log("query ParticipationQuery", ParticipationQuery);
            dbContext.getQuery(ParticipationQuery, parameters, false, function(error, data) {
                console.log("data", data);
                console.log("error", error);
                if (error != null) {
                    return res.sendStatus(401).send({ "message": error })
                }
                if (data) {
                    return res.json(response(data, { "message": data }));
                } else {
                    return res.status(200).send('');
                }
            });

        } else {
            return res.sendStatus(401);
        }

    }


    function startParticipation(req, res) {


        var par = JSON.parse(JSON.stringify(req.body));

        //  var par = jwt.decode(req.token);
        //   console.log("decoded", decoded);
        // console.log("par.LoginID", par.LoginID);
        //need to do properly
        console.log("*****************startParticipation*****************", par);
        var userInfo;

        if (req.token != undefined) {
            userInfo = jwt.decode(req.token);
        } else {
            return res.sendStatus(401).send('missing token');
        }
        console.log("userInfo", userInfo);
        //  var planningDB = "MSS_PSA.";
        //  if (par.TaskName != undefined && par.FromDate != undefined && par.FromTime != undefined && par.ParticipantEmployeeID) {
        if (par.TaskName != undefined && par.StartTime != undefined && par.EndTime != undefined && par.ParticipantEmployeeID) {
            var parameters = [];
            var d = new Date();
            var n = (d.getTimezoneOffset() / 60) * (-1); //to fix timezone difference
            var timeInMss = new Date().addHours(n).toISOString().slice(0, 19).replace('T', ' ');
            console.log("timeInMss", timeInMss);
            parameters.push({ name: 'TaskName', type: TYPES.NVarChar, val: par.TaskName });
            parameters.push({ name: 'StartTime', type: TYPES.Date, val: par.StartTime });
            parameters.push({ name: 'EndTime', type: TYPES.Date, val: par.EndTime });
            parameters.push({ name: 'ParticipantEmployeeID', type: TYPES.NVarChar, val: par.ParticipantEmployeeID });
            // parameters.push({ name: 'CurrentTime', type: TYPES.DateTime, val: timeInMss });
            parameters.push({ name: 'Action', type: TYPES.NVarChar, val: 'Start' });
            parameters.push({ name: 'SenderId', type: TYPES.NVarChar, val: userInfo.EmployeeId });

            var ParticipationQuery = "insert into ActionParticipation(ManpowerID,Action,Time,SenderId) " +
                " select (select m.ManpowerID from ManpowerDetail m  " +
                "inner join EventDetail e on m.EventId = e.EventId " +
                " where m.EmployeeId = @ParticipantEmployeeID " +
                " and e.TaskName = @TaskName " +
                " and e.StartTime = '" + par.StartTime + "'  " +
                " and e.EndTime = '" + par.EndTime + "'  " +
                " ) as ManpowerID, @Action,'" + timeInMss + "'," + userInfo.EmployeeId + "";
            //check if there is no record in the Participation Table
            ParticipationQuery = ParticipationQuery +
                " WHERE not exists ( select * from ActionParticipation where ManpowerId = (" +
                " select m.ManpowerID from ManpowerDetail m  " +
                "inner join EventDetail e on m.EventId = e.EventId " +
                " where m.EmployeeID = @ParticipantEmployeeID " +
                " and e.TaskName = @TaskName " +
                " and e.StartTime = '" + par.StartTime + "'  " +
                " and e.EndTime = '" + par.EndTime + "'  " +
                " ) );" +
                " select * from ActionParticipation where ManpowerID  = " +
                " (select m.ManpowerID from ManpowerDetail m  " +
                "inner join EventDetail e on m.EventId = e.EventId " +
                " where m.EmployeeID = @ParticipantEmployeeID " +
                " and e.TaskName = @TaskName " +
                " and e.StartTime = '" + par.StartTime + "'  " +
                " and e.EndTime = '" + par.EndTime + "'  " +
                " )"
            console.log("query ParticipationQuery", ParticipationQuery);
            dbContext.getQuery(ParticipationQuery, parameters, false, function(error, data) {
                console.log("data", data);
                console.log("error", error);
                if (error != null) {
                    return res.sendStatus(401).send({ "message": error })
                }
                if (data) {
                    return res.json(response(data, { "message": data }));
                } else {
                    return res.status(200).send('');
                }
            });

        } else {
            return res.sendStatus(401);
        }

    }


    function getRemark(req, res) {

        var par = JSON.parse(JSON.stringify(req.body));
        console.log("getRemark******************************", par);
        if (req.token != undefined) {
            userInfo = jwt.decode(req.token);
        } else {
            return res.sendStatus(401).send('missing token');
        }
        console.log("userInfo", userInfo);

        console.log("getRemark******************************");
        if (par.TaskName != undefined && par.StartTime != undefined && par.EndTime != undefined) {


            var parameters = [];
            parameters.push({ name: 'TaskName', type: TYPES.NVarChar, val: par.TaskName });
            if (par.ParticipantEmployeeID != undefined) {
                parameters.push({ name: 'ParticipantEmployeeID', type: TYPES.NVarChar, val: par.ParticipantEmployeeID });
            }

            // parameters.push({ name: 'CurrentTime', type: TYPES.DateTime, val: timeInMss });
            parameters.push({ name: 'Remark', type: TYPES.NVarChar, val: par.Remark });
            parameters.push({ name: 'CreatorId', type: TYPES.NVarChar, val: userInfo.EmployeeId });

            var ParticipationQuery = "";
            if (par.ParticipantEmployeeID != undefined) {
                ParticipationQuery = "select a.*, b.DisplayName as CreatorName from RemarkToManpowerDetail a" +
                    " inner join " + constant.planning + "[dbo].[View_MSS_GetSkills] b on a.CreatorID = b.EmployeeId " +
                    " where a.ManpowerID =(" +
                    "select m.ManpowerID from ManpowerDetail m  " +
                    "inner join EventDetail e on m.EventId = e.EventId " +
                    " where m.EmployeeID = @ParticipantEmployeeID " +
                    " and e.TaskName = @TaskName " +
                    " and e.StartTime = '" + par.StartTime + "'  " +
                    " and e.EndTime = '" + par.EndTime + "'  " +
                    " ) order by a.Time";
            } else {
                ParticipationQuery = "select distinct a.*, b.DisplayName as CreatorName from RemarkToManpowerDetail a" +
                    " inner join " + constant.planning + "[dbo].[View_MSS_GetSkills] b on a.CreatorID = b.EmployeeId " +
                    " where a.ManpowerID =(" +
                    "select m.ManpowerID from ManpowerDetail m  " +
                    "inner join EventDetail e on m.EventId = e.EventId " +
                    " where " +
                    " e.TaskName = @TaskName " +
                    " and e.StartTime = '" + par.StartTime + "'  " +
                    " and e.EndTime = '" + par.EndTime + "'  " +
                    " ) order by a.Time";
            }
            console.log("ParticipationQuery****************************", ParticipationQuery);
            dbContext.getQuery(ParticipationQuery, parameters, false, function(error, data) {
                console.log("error", error);
                console.log("data", data);
                if (error != null) {
                    return res.sendStatus(401).send({ "message": error })
                }
                if (data) {
                    return res.json(response(data, { "message": data }));
                } else {
                    return res.json(response([], { "message": [] }));
                }
            });


        } else {
            return res.sendStatus(401);
        }
    }


    function addRemark(req, res) {
        var par = JSON.parse(JSON.stringify(req.body));

        console.log("addremark", par);

        if (req.token != undefined) {
            userInfo = jwt.decode(req.token);
        } else {
            return res.sendStatus(401).send('missing token');
        }
        console.log("userInfo", userInfo);

        if (par.Remark != undefined && par.TaskName != undefined && par.StartTime != undefined && par.EndTime != undefined && par.ParticipantEmployeeID != undefined) {
            console.log("remark length", par.Remark.length);
            if (par.Remark.length >= 255) {
                return res.status(200).send('Remark cannot be more than 255 characters');
            }
            var parameters = [];
            var d = new Date();
            var n = (d.getTimezoneOffset() / 60) * (-1); //to fix timezone difference
            var timeInMss = new Date().addHours(n).toISOString().slice(0, 19).replace('T', ' ');
            console.log("timeInMss", timeInMss);
            parameters.push({ name: 'TaskName', type: TYPES.NVarChar, val: par.TaskName });
            parameters.push({ name: 'ParticipantEmployeeID', type: TYPES.NVarChar, val: par.ParticipantEmployeeID });
            // parameters.push({ name: 'CurrentTime', type: TYPES.DateTime, val: timeInMss });
            parameters.push({ name: 'Remark', type: TYPES.NVarChar, val: par.Remark });
            parameters.push({ name: 'CreatorId', type: TYPES.NVarChar, val: userInfo.EmployeeId });


            var ParticipationQuery = "insert into RemarkToManpowerDetail(ManpowerID,Remark,Time,CreatorId) " +
                " select (select m.ManpowerID from ManpowerDetail m  " +
                "inner join EventDetail e on m.EventId = e.EventId " +
                " where m.EmployeeID = @ParticipantEmployeeID " +
                " and e.TaskName = @TaskName " +
                " and e.StartTime = '" + par.StartTime + "'  " +
                " and e.EndTime = '" + par.EndTime + "'  " +
                " ) as ManpowerID, @Remark,'" + timeInMss + "',@CreatorId ;";
            ParticipationQuery = ParticipationQuery + "select a.*, b.DisplayName as CreatorName from RemarkToManpowerDetail a" +
                " inner join ManpowerDetail b on a.CreatorID = b.ManpowerID" +
                " where a.ManpowerID =(" +
                "select m.ManpowerID from ManpowerDetail m  " +
                "inner join EventDetail e on m.EventId = e.EventId " +
                " where m.EmployeeID = @ParticipantEmployeeID " +
                " and e.TaskName = @TaskName " +
                " and e.StartTime = '" + par.StartTime + "'  " +
                " and e.EndTime = '" + par.EndTime + "'  " +
                " ) order by a.Time";

            console.log("ParticipationQuery", ParticipationQuery);
            dbContext.getQuery(ParticipationQuery, parameters, false, function(error, data) {
                console.log("*****************data", data);
                console.log("******************error", error);
                if (error != null) {
                    return res.sendStatus(401).send({ "message": error })
                }
                if (data) {
                    return res.json(response(data, { "message": data }));
                } else {
                    return res.status(200).send('');
                }
            });


        } else {
            return res.sendStatus(401);
        }
    }

    function endParticipation(req, res) {
        // try {

        var par = JSON.parse(JSON.stringify(req.body));
        console.log("*************endParticipation", par);

        if (req.token != undefined) {
            userInfo = jwt.decode(req.token);
        } else {
            return res.sendStatus(401).send('missing token');
        }
        console.log("userInfo", userInfo);

        if (par.TaskName != undefined && par.StartTime != undefined && par.EndTime != undefined && par.ParticipantEmployeeID) {
            //  if (par.TaskName != undefined && par.FromDate != undefined && par.FromTime != undefined && par.ParticipantID) {
            var parameters = [];
            var d = new Date();
            var n = (d.getTimezoneOffset() / 60) * (-1); //to fix timezone difference
            var timeInMss = new Date().addHours(n).toISOString().slice(0, 19).replace('T', ' ');
            console.log("timeInMss", timeInMss);
            parameters.push({ name: 'TaskName', type: TYPES.NVarChar, val: par.TaskName });
            // parameters.push({ name: 'StartTime', type: TYPES.Date, val: par.StartTime });
            // parameters.push({ name: 'EndTime', type: TYPES.Date, val: par.EndTime });
            parameters.push({ name: 'ParticipantEmployeeID', type: TYPES.NVarChar, val: par.ParticipantEmployeeID });
            // parameters.push({ name: 'CurrentTime', type: TYPES.DateTime, val: timeInMss });
            parameters.push({ name: 'Action', type: TYPES.NVarChar, val: 'End' });
            parameters.push({ name: 'SenderId', type: TYPES.NVarChar, val: userInfo.EmployeeId });

            var ParticipationQuery = "insert into ActionParticipation(ManpowerID,Action,Time,SenderId) " +
                " select (select m.ManpowerID from ManpowerDetail m  " +
                "inner join EventDetail e on m.EventId = e.EventId " +
                " where m.EmployeeID = @ParticipantEmployeeID " +
                " and e.TaskName = @TaskName " +
                " and e.StartTime = '" + par.StartTime + "'  " +
                " and e.EndTime = '" + par.EndTime + "'  " +
                " ) as ManpowerID, @Action,'" + timeInMss + "',@SenderId";
            ParticipationQuery = ParticipationQuery +
                " WHERE exists ( select * from ActionParticipation where Action = 'Start' and ManpowerId = (" +
                " select m.ManpowerID from ManpowerDetail m  " +
                " inner join EventDetail e on m.EventId = e.EventId " +
                " where m.EmployeeID = @ParticipantEmployeeID " +
                " and e.TaskName = @TaskName " +
                " and e.StartTime = '" + par.StartTime + "'  " +
                " and e.EndTime = '" + par.EndTime + "'  " +
                " ));" +
                " select * from ActionParticipation where ManpowerID  = " +
                " (select m.ManpowerID from ManpowerDetail m  " +
                "inner join EventDetail e on m.EventId = e.EventId " +
                " where m.EmployeeID = @ParticipantEmployeeID " +
                " and e.TaskName = @TaskName " +
                " and e.StartTime = '" + par.StartTime + "'  " +
                " and e.EndTime = '" + par.EndTime + "'  " +
                " )";

            console.log("end participation query", ParticipationQuery);

            dbContext.getQuery(ParticipationQuery, parameters, false, function(error, data) {
                console.log("*****************data", data);
                console.log("******************error", error);
                if (error != null) {
                    return res.sendStatus(401).send({ "message": error })
                }
                if (data) {
                    return res.json(response(data, { "message": data }));
                } else {
                    return res.status(200).send('');
                }
            });
        } else {
            return res.sendStatus(401);
        }
        // } catch (e) {
        //     return res.sendStatus(500).send(String(e));
        // }
    }
    return {
        startEvent: startEvent,
        startParticipation: startParticipation,
        endParticipation: endParticipation,
        addRemark: addRemark,
        getRemark: getRemark
    };
}

module.exports = EventRepository;