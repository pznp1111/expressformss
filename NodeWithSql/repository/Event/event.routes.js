const EventRepository = require('./Event.respository');
const dbContext = require('../../Database/dbContext');

module.exports = function(router) {
    const eventRepository = EventRepository(dbContext);
    router.route('/startEvent')
        .post(eventRepository.startEvent);
    router.route('/startParticipation')
        .post(eventRepository.startParticipation);
    router.route('/endParticipation')
        .post(eventRepository.endParticipation);
    router.route('/addRemark')
        .post(eventRepository.addRemark);
    router.route('/getRemark')
        .post(eventRepository.getRemark);
}