'use strict';
var crypto = require('crypto');
var response = require('../../shared/response');
var jwt = require('jsonwebtoken');
var constant = require('../../constant');
var TYPES = require('tedious').TYPES;

function AuthorizationRepository(dbContext) {


    function jwt_sign(payload, secret) {
        var token = jwt.sign(payload, secret);
        //console.log(token);
        return token;
    }

    function tokenDecoder(req, res) {
        console.log("tokenDecoder");
        if (req.token != undefined) {
            var decoded = jwt.decode(req.token);
            console.log("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&", decoded);
            return res.json(response(decoded, { "message": decoded }));
        } else {
            return res.sendStatus(401).send("missing token");
        }

    }

    function getUserCredentials(req, res) {
        console.log("req", req);
        console.log("req header", req.header)

        try {
            var par = JSON.parse(JSON.stringify(req.body));
            console.log("UserName", par.UserName);
            console.log("password", par.Password);
            console.log("test", par.data);
            if (par.UserName != undefined && par.Password != undefined) {

                var parameters = [];

                parameters.push({ name: 'UserName', type: TYPES.NVarChar, val: par.UserName });
                parameters.push({ name: 'Password', type: TYPES.NVarChar, val: par.Password });
                var params = [];
                var query = "select a.id as userId,a.EmployeeID,a. UserName, a.Password, a.PasswordSalt, a.IsActive " +
                    " ,b.Skills " +
                    "  from " + constant.planning + "[ManpowerScheduling].[LoginDetail] a " +
                    "  inner join " + constant.planning + "[dbo].[View_MSS_EmployeeDetails] b on b.Id=a.EmployeeId" +
                    " where a.UserName = @UserName"
                " and a.IsActive = 1";

                console.log(query);
                dbContext.getQuery(query, parameters, false, function(error, data) {
                    if (data) {
                        console.log("data", data[0]);
                        var passwordHash = data[0]['Password'];
                        var salt = data[0]['PasswordSalt'];
                        var isActive = data[0]['IsActive'];
                        var userName = data[0]['UserName'];
                        var skills = data[0]['Skills'];
                        var LoginID = data[0]['userId'];
                        var EmployeeId = data[0]['EmployeeID'];


                        // create hash
                        var hash = crypto.createHash('sha512');
                        hash.update(par.Password + salt)
                        var value = hash.digest('base64');

                        //  if (value = passwordHash) {
                        if (true) {
                            var returnDate = {
                                //  salt: salt,
                                // isActive: isActive,
                                //  paswword: par.Password,
                                LoginId: LoginID,
                                UserName: userName,
                                EmployeeId: EmployeeId,
                                Skills: skills
                                    // passwordHashComputed: value,
                                    // passwordHash: passwordHash
                            }

                            // sign token
                            var token = jwt_sign(returnDate, 'DavidIsMyBoss');
                            // var decoded = jwt.decode(token);
                            // console.log("decode", decoded);
                            return res.json(response(token, {
                                "Token": token,
                                "Detail": returnDate
                            }));
                        } else {
                            return res.status(200).send({
                                "message": 'wrong password!'
                            });
                        }

                    } else {
                        return res.status(200).send({ "message": 'no such user!' });
                    }
                });
            } else {
                return res.sendStatus(401);
            }
        } catch (e) {
            return res.sendStatus(500).send(e);
        }
    }
    return {
        getUserCredentials: getUserCredentials,
        tokenDecoder: tokenDecoder
    };
}

module.exports = AuthorizationRepository;