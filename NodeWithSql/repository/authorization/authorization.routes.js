const AuthorizationRepository = require('./authorization.respository');
const dbContext = require('../../Database/dbContext');

module.exports = function(router) {
    const authorizationRepository = AuthorizationRepository(dbContext);
    router.route('/authorization')
        .post(authorizationRepository.getUserCredentials);
    router.route('/tokenDecoder')
        .post(authorizationRepository.tokenDecoder);
    // router.route('/login')
    //     .post(authorizationRepository.login_post);
}