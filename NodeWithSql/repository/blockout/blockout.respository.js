var response = require('../../shared/response');

var TYPES = require('tedious').TYPES;
var jwt = require('jsonwebtoken');

var constant = require('../../constant');

function BlockoutRepository(dbContext) {


    function addblockout(req, res) {
        var par = JSON.parse(JSON.stringify(req.body));
        var userInfo;
        if (req.token != undefined) {
            userInfo = jwt.decode(req.token);
        } else {
            return res.sendStatus(401).send('missing token');
        }
        if (par.BlockOutStartTime != undefined && par.BlockOutEndTime != undefined && par.StartTime != undefined && par.EndTime != undefined && par.ParticipantEmployeeID != undefined) {
            var remark = "";
            if (par.Remark != undefined) {
                remark = par.Remark;
            }
            if (str.length > 254) {
                return res.sendStatus("remark cannot be more than 255 characters");
            }

            var parameters = [];
            var d = new Date();
            var n = (d.getTimezoneOffset() / 60) * (-1); //to fix timezone difference
            var timeInMss = d.addHours(n); //.toISOString().slice(0, 19).replace('T', ' ');
            console.log("timeInMss", timeInMss);
            parameters.push({ name: 'ParticipantEmployeeID', type: TYPES.NVarChar, val: par.ParticipantEmployeeID });
            // parameters.push({ name: 'StartTime', type: TYPES.Date, val: par.StartTime });
            // parameters.push({ name: 'EndTime', type: TYPES.Date, val: par.EndTime });
            // parameters.push({ name: 'BlockOutStartTime', type: TYPES.Date, val: par.BlockOutStartTime });
            // parameters.push({ name: 'BlockOutEndTime', type: TYPES.Date, val: par.BlockOutEndTime });
            parameters.push({ name: 'Remark', type: TYPES.NVarChar, val: Remark });
            parameters.push({ name: 'CreateTime', type: TYPES.Date, val: timeInMss });
            parameters.push({ name: 'SenderId', type: TYPES.NVarChar, val: userInfo.userId });



            var query = "insert into ActionBlockOut(BlockOutManpowerID,StartTime,EndTime,Remark,CreateTime,SenderId)" +
                " VALUES (select (select m.ManpowerID from ManpowerDetail m  " +
                "inner join EventDetail e on m.EventId = e.EventId " +
                " where m.EmployeeId = @ParticipantEmployeeID " +
                " and e.TaskName = @TaskName " +
                " and e.StartTime = '" + par.StartTime + "'  " +
                " and e.EndTime = '" + par.EndTime + "'  " +
                " ) as ManpowerID,'" + par.BlockOutStartTime + "','" + par.BlockOutEndTime + "',@Remark,@CreateTime,@SenderId);" +

                "select * from ActionBlockOut where BlockOutManpowerID = (select m.ManpowerID from ManpowerDetail m  " +
                "inner join EventDetail e on m.EventId = e.EventId " +
                " where m.EmployeeId = @ParticipantEmployeeID " +
                " and e.TaskName = @TaskName " +
                " and e.StartTime = '" + par.StartTime + "'  " +
                " and e.EndTime = '" + par.EndTime + "'  " +
                " );";

            dbContext.getQuery(query, parameters, false, function(error, data) {
                if (data) {
                    return res.json(response("completed", data));
                } else {
                    return res.status(200).send('');
                }
            });
        } else {
            return res.sendStatus(401);
        }
    }

    function removeblockout(req, res) {
        var par = JSON.parse(JSON.stringify(req.body));
        var userInfo;
        if (req.token != undefined) {
            userInfo = jwt.decode(req.token);
        } else {
            return res.sendStatus(401).send('missing token');
        }
        if (par.BlockOutStartTime != undefined && par.BlockOutEndTime != undefined && par.StartTime != undefined && par.EndTime != undefined && par.ParticipantEmployeeID != undefined) {



            var parameters = [];
            var d = new Date();
            var n = (d.getTimezoneOffset() / 60) * (-1); //to fix timezone difference
            var timeInMss = new Date().addHours(n); //.toISOString().slice(0, 19).replace('T', ' ');
            console.log("timeInMss", timeInMss);
            parameters.push({ name: 'ParticipantEmployeeID', type: TYPES.NVarChar, val: par.ParticipantEmployeeID });
            // parameters.push({ name: 'StartTime', type: TYPES.Date, val: par.StartTime });
            // parameters.push({ name: 'EndTime', type: TYPES.Date, val: par.EndTime });
            parameters.push({ name: 'Remark', type: TYPES.NVarChar, val: Remark });
            parameters.push({ name: 'CreateTime', type: TYPES.Date, val: timeInMss });
            parameters.push({ name: 'SenderId', type: TYPES.NVarChar, val: userInfo.userId });



            var query =
                "delete from ActionBlockOut where BlockOutManpowerID = (select m.ManpowerID from ManpowerDetail m  " +
                "inner join EventDetail e on m.EventId = e.EventId " +
                " where m.EmployeeId = @ParticipantEmployeeID " +
                " and e.TaskName = @TaskName " +
                " and e.StartTime = '" + par.StartTime + "'  " +
                " and e.EndTime = '" + par.EndTime + "'  " +
                " ) and StartTime = '" + par.BlockOutStartTime + "' and EndTime = '" + par.BlockOutEndTime + "';";

            dbContext.getQuery(query, parameters, false, function(error, data) {
                if (data) {
                    return res.json(response("completed", data));
                } else {
                    return res.status(200).send('');
                }
            });
        } else {
            return res.sendStatus(401);
        }
    }
    return {
        addblockout: addblockout,
        removeblockout: removeblockout
    };
}

module.exports = BlockoutRepository;