const BlockoutRepository = require('./blockout.respository');
const dbContext = require('../../Database/dbContext');

module.exports = function(router) {
    const blockoutRepository = BlockoutRepository(dbContext);
    router.route('/addblockout')
        .post(blockoutRepository.addblockout);
    router.route('/removeblockout')
        .post(blockoutRepository.removeblockout);
}