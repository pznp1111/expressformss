var response = require('../../shared/response');

var TYPES = require('tedious').TYPES;
var jwt = require('jsonwebtoken');

var constant = require('../../constant');

function GetTaskRepository(dbContext) {
    function getTaskList(req, res) {
        try {
            console.log("planning", constant.planning);
            console.log("received body", req.body);
            // console.log("received header", req.header);
            var par = JSON.parse(JSON.stringify(req.body));
            var par = jwt.decode(req.token);
            //   console.log("decoded", decoded);
            //  console.log("par", par);
            console.log("par.EmployeeId", par.EmployeeId);

            //var planningDB = "MSS_PSA.";
            //need to do properly

            if (par.EmployeeId != undefined) {

                var parameters = [];
                parameters.push({ name: 'EmployeeID', type: TYPES.NVarChar, val: par.EmployeeId });



                var query = "select(select " +
                    " (select a.TaskName from " + constant.planning + "dbo.View_MSS_GetSkills a where a.TaskId = p.TaskId and  DATEADD(day,DATEDIFF(day, 0, a.FromDate),CAST(a.FromTime AS DATETIME)) = p.StartTime  and DATEADD(day,DATEDIFF(day, 0, a.ToDate),CAST(a.ToTime AS DATETIME)) = p.EndTime  ) as TaskName, p.EventId,p.TaskId, " +
                    " Convert(date,p.StartTime) as FromDate,Convert(time,p.StartTime) as FromTime, " +
                    " Convert(date,p.EndTime) as ToDate,Convert(time,p.EndTime) as ToTime " +
                    ",p.EndTime,p.SupervisorId,p.LocationPostalCode,p.Status from " +
                    " EventUserParticipation p " +
                    " inner join ManpowerDetail b on b.EventId = p.EventId " +
                    "  where b.EmployeeID = @EmployeeID " +
                    "   for json path " +
                    "  ) as MTSData, " +
                    " ( " +
                    "  SELECT distinct a.TaskName,a.TaskId,a.FromDate,a.FromTime,a.ToDate,a.ToTime,c.PostalCode,'Pending' as Status " +
                    "   FROM " + constant.planning + "[dbo].[View_MSS_GetSkills] a inner join " + constant.planning + "[ManpowerScheduling].[Location] c on a.LocationId = c.id   " +
                    //  "a.TaskId not in (select distinct TaskId from EventDetail)  " +
                    " where not exists(select distinct t.TaskId,t.StartTime,t.endtime from EventDetail t where t.TaskId = a.TaskId and " +
                    "  DATEADD(day,DATEDIFF(day, 0, a.FromDate),CAST(a.FromTime AS DATETIME)) = t.startTime and " +
                    "  DATEADD(day,DATEDIFF(day, 0, a.ToDate),CAST(a.ToTime AS DATETIME)) = t.EndTime " +
                    " ) " +
                    " and a.EmployeeID = @EmployeeID " +
                    "  for json path " +
                    " ) as MSSData ";
                console.log("query", query);

                dbContext.getQuery(query, parameters, false, function(error, data) {
                    console.log("data0", data);
                    // console.log("data", JSON.parse(data));
                    if (data != undefined && data != null) {
                        var MTSData = data[0].MTSData;
                        var MSSData = data[0].MSSData;

                        console.log("MTSData", MTSData);
                        console.log("MSSData", MSSData);

                        var result;
                        if (MTSData != null && MSSData != null) {
                            result = JSON.parse(MTSData).concat(JSON.parse(MSSData));
                        }
                        if (MTSData == null) {
                            result = JSON.parse(MSSData);
                        }
                        if (MSSData == null) {
                            result = JSON.parse(MTSData);
                        }
                        console.log("result", result);
                        return res.json(response(result, { "message": result }));
                    } else {
                        return res.sendStatus(401);
                    }
                });

            } else {
                return res.sendStatus(401);
            }
        } catch (e) {
            return res.sendStatus(500).send(e);
        }


    }


    function getTaskListRecent(req, res) {

        //redo

        console.log("constant", constant);
        //  try {
        console.log("received body", req.body);
        // console.log("received header", req.header);
        var par = JSON.parse(JSON.stringify(req.body));
        //  var par = jwt.decode(par.token);
        //   console.log("decoded", decoded);
        console.log("par", par);
        console.log("par.LoginID", par.LoginID);
        //type can be: "Day Week Month"
        if (par.LoginID != undefined && par.Type != undefined) {

            if (par.Type != "Day" && par.Type != "Week" && par.Type != "Month") {
                return res.sendStatus(401).send('type must be: "Day/Week/Month');
            }

            var startDate = "";
            var endDate = "";
            var d = new Date();
            var n = (d.getTimezoneOffset() / 60) * (-1); //to fix timezone difference
            var startDate1 = new Date().addHours(n);
            var endDate1 = new Date().addHours(n);

            //  startDate = startDate1.toISOString().slice(0, 19).replace('T', ' ');


            console.log();


            console.log("1", par.Type);
            if (par.Type == "Day") {
                endDate1.setHours(23, 59, 59, 999);
                //  endDate = endDate1.toISOString().slice(0, 19).replace('T', ' ');

            } else if (par.Type == "Week") {
                endDate1.setDate(endDate1.getDate() + 6);
                endDate1.setHours(23, 59, 59, 999);
                endDate = endDate1.toISOString().slice(0, 19).replace('T', ' ');
            } else if (par.Type == "Month") {
                endDate1 = new Date(endDate1.getFullYear(), endDate1.getMonth() + 1, 0);

                // endDate1.setDate(endDate1.getDate() + 30);
                endDate1.setHours(23, 59, 59, 999);
                endDate = endDate1.toISOString().slice(0, 19).replace('T', ' ');
                //  endDate.setDate(startDate.getDate() + 30).setHours(23, 59, 59, 999).toISOString().slice(0, 19).replace('T', ' ');
            }

            console.log("startDate", startDate);
            console.log("endDate", endDate);
            var parameters = [];
            parameters.push({ name: 'EmployeeID', type: TYPES.NVarChar, val: par.EmployeeID });
            parameters.push({ name: 'startDateTime', type: TYPES.DateTime, val: startDate1 });
            parameters.push({ name: 'endDateTime', type: TYPES.DateTime, val: endDate1 });


            // var query = " SELECT distinct a.TaskName,a.TaskId,a.FromDate,a.FromTime,a.ToDate,a.ToTime,c.PostalCode," +
            //     "(select EmployeeId,DisplayName,Skill from " + constant.planning + "[dbo].[View_MSS_GetSkills] where taskName = a.TaskName FOR JSON PATH) as Detail " +
            //     "   FROM " + constant.planning + "[dbo].[View_MSS_GetSkills] a " +
            //     "	inner join " + constant.planning + "[ManpowerScheduling].[Location] c on a.LocationId = c.id" +
            //     "  where a.UserName = @LoginID " +
            //     " and @startDateTime >= DATEADD(day,DATEDIFF(day, 0, a.FromDate),CAST(a.FromTime AS DATETIME)) "
            // " and @endDateTime <= DATEADD(day,DATEDIFF(day, 0, a.ToDate),CAST(a.ToTime AS DATETIME)) "
            // "	and a.isActive = 1 " +
            // "	order by FromDate ";



            var query = "select(select " +
                " (select a.TaskName from " + constant.planning + "dbo.View_MSS_GetSkills a where a.TaskId = p.TaskId " +
                " and  DATEADD(day,DATEDIFF(day, 0, a.FromDate),CAST(a.FromTime AS DATETIME)) = p.StartTime  and DATEADD(day,DATEDIFF(day, 0, a.ToDate),CAST(a.ToTime AS DATETIME)) = p.EndTime  ) as TaskName, p.EventId,p.TaskId,p.StartTime,p.EndTime,p.SupervisorId,p.LocationPostalCode,p.Status from " +
                " EventUserParticipation p " +
                " inner join ManpowerDetail b on b.EventId = p.EventId " +
                " inner join EventDetail c on c.EventId = p.EventID " +
                "  where b.EmployeeID = @EmployeeID  " +
                " and @startDateTime >= c.StartTime " +
                " and @endDateTime <= c.EndTime " +
                "   for json path " +
                "  ) as MTSData, " +
                " ( " +
                "  SELECT distinct a.TaskName,a.TaskId,a.FromDate,a.FromTime,a.ToDate,a.ToTime,c.PostalCode,'Pending' as Status " +
                "   FROM " + constant.planning + "[dbo].[View_MSS_GetSkills] a inner join " + constant.planning + "[ManpowerScheduling].[Location] c on a.LocationId = c.id   " +
                " where a.TaskId not in (select distinct TaskId from EventDetail)  " +
                " and a.EmployeeID = @EmployeeID " +
                " and @startDateTime >= DATEADD(day,DATEDIFF(day, 0, a.FromDate),CAST(a.FromTime AS DATETIME)) " +
                " and @endDateTime <= DATEADD(day,DATEDIFF(day, 0, a.ToDate),CAST(a.ToTime AS DATETIME)) " +
                "  for json path " +
                " ) as MSSData ";
            console.log("query", query);

            dbContext.getQuery(query, parameters, false, function(error, data) {

                if (data != undefined && data != null) {
                    var MTSData = data[0].MTSData;
                    var MSSData = data[0].MSSData;

                    // console.log("MTSData", MTSData);
                    // console.log("MSSData", MSSData);

                    var result;
                    if (MTSData != null && MSSData != null) {
                        result = JSON.parse(MTSData).concat(JSON.parse(MSSData));
                    }
                    if (MTSData == null) {
                        result = JSON.parse(MSSData);
                    }
                    if (MSSData == null) {
                        result = JSON.parse(MTSData);
                    }
                    return res.json(response(data, { "message": result }));
                } else {
                    return res.sendStatus(401);
                }

                // console.log("error", error);
                // return res.json(response(data, { "message": data }));
            });

        } else {
            return res.sendStatus(401);
        }
    }


    function getTaskDetail(req, res) {
        var par = JSON.parse(JSON.stringify(req.body));
        console.log("par", par);
        if (par.TaskId != undefined && par.StartTime != undefined && par.EndTime != undefined) {


            // var startTime = Date.parse(par.StartTime);
            // var endTime = Date.parse(par.StartTime);

            var queryMTS = "select " +
                // "(select a.TaskName from MSS_PSA.dbo.View_MSS_GetSkills a where a.TaskId = p.TaskId " +
                // "and  " +
                // "DATEADD(day,DATEDIFF(day, 0, a.FromDate),CAST(a.FromTime AS DATETIME)) = p.StartTime " +
                // " and DATEADD(day,DATEDIFF(day, 0, a.ToDate),CAST(a.ToTime AS DATETIME)) = p.EndTime " +
                // " ) " +
                // "as TaskName, " +
                "a.TaskName," +
                " p.EventId,p.TaskId,p.StartTime,p.EndTime,p.SupervisorId,p.LocationPostalCode " +
                ",p.Detail " +
                "from EventUserParticipation p inner join " + constant.planning + "[dbo].[View_MSS_GetSkills] a on a.TaskId = p.TaskId and " +
                "DATEADD(day,DATEDIFF(day, 0, a.FromDate),CAST(a.FromTime AS DATETIME)) = p.StartTime " +
                " and DATEADD(day,DATEDIFF(day, 0, a.ToDate),CAST(a.ToTime AS DATETIME)) = p.EndTime " +
                "where p.TaskId = @TaskId and p.StartTime = '" + par.StartTime + "'and p.EndTime = '" + par.EndTime + "'";


            var queryMSS = " SELECT distinct a.TaskName,a.TaskId,a.FromDate,a.FromTime,a.ToDate,a.ToTime,c.PostalCode," +
                "(select EmployeeId,DisplayName,Skill, 'Pending' as status from " + constant.planning + "[dbo].[View_MSS_GetSkills] where taskId = @TaskId and " +
                " '" + par.StartTime + "'= DATEADD(day,DATEDIFF(day, 0, FromDate),CAST(FromTime AS DATETIME))" +
                " FOR JSON PATH) as Detail " +
                "   FROM " + constant.planning + "[dbo].[View_MSS_GetSkills] a " +
                "	inner join " + constant.planning + "[ManpowerScheduling].[Location] c on a.LocationId = c.id" +
                "  where a.TaskId = @TaskId " +
                " and '" + par.StartTime + "'= DATEADD(day,DATEDIFF(day, 0, a.FromDate),CAST(a.FromTime AS DATETIME)) "
            " and '" + par.EndTime + "'= DATEADD(day,DATEDIFF(day, 0, a.ToDate),CAST(a.ToTime AS DATETIME)) "
            "	and a.isActive = 1 " +
            "	order by FromDate ";

            var query = "IF EXISTS (" + queryMTS + " ) BEGIN " + queryMTS + " END ELSE " +
                "BEGIN " + queryMSS + " END";


            var parameters = [];
            parameters.push({ name: 'TaskId', type: TYPES.NVarChar, val: par.TaskId });
            // parameters.push({ name: 'startTime', type: TYPES.Date, val: startTime });
            // parameters.push({ name: 'endTime', type: TYPES.Date, val: endTime });
            console.log("query", query);

            dbContext.getQuery(query, parameters, false, function(error, data) {
                console.log("error", error);
                return res.json(response(data, { "message": data }));
            });

        } else {
            return res.sendStatus(401);
        }
    }


    function getWorkingDatesByMonth(req, res) {

        //redo
        console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%getWorkingDatesByMonth body", req.body);
        // console.log("received header", req.header);
        var par = JSON.parse(JSON.stringify(req.body));
        //  var par = jwt.decode(par.token);
        //   console.log("decoded", decoded);
        console.log("par", par);
        // console.log("par.startDate", par.startDate);

        var userInfo;
        if (req.token != undefined) {
            console.log("userInfo.EmployeeId1", req.token);
            userInfo = jwt.decode(req.token);
            console.log("user", userInfo);
            console.log("userInfo.EmployeeId2", userInfo);
        } else {
            return res.sendStatus(401).send('missing token');
        }

        console.log("userInfo.EmployeeId", userInfo);


        if (userInfo.EmployeeId != undefined && par.year != undefined && par.month != undefined) {

            //start and end of month
            var date = new Date(); //, y = date.getFullYear(), m = date.getMonth();
            var startDate;
            if (par.month.toString().length != 2) {
                startDate = par.year + "-0" + par.month + "-01 00:00:000";
            } else {
                startDate = par.year + "-" + par.month + "-01 00:00:000";
            }

            // var temp = Date.parse(startDate);
            var endDate;
            // if (((par.month) + 1).toString().length != 2) {
            //     endDate = par.year + "-0" + (par.month + 1) + "-01 00:00:000";
            // } else {
            //     startDate = par.year + "-" + (par.month + 1) + "-01 00:00:000";
            // }
            endDate = new Date(par.year, par.month, 0);
            endDate.setHours(23, 59, 59, 999);
            endDate = endDate.toISOString().slice(0, 19).replace('T', ' ');

            console.log("startdate", startDate);

            console.log("endDate", endDate);

            var parameters = [];


            parameters.push({ name: 'EmployeeID', type: TYPES.NVarChar, val: userInfo.EmployeeId });


            var query = " select Convert(date,p.StartTime) as FromDate " +
                "  from  EventUserParticipation p  inner join ManpowerDetail b on b.EventId = p.EventId   " +
                "  where b.EmployeeID = 360   " +
                "  and  p.StartTime >='" + startDate + "'  and p.StartTime <='" + endDate + "'   " +
                "   union  " +
                "   SELECT distinct a.FromDate   " +
                "  FROM " + constant.planning + "[dbo].[View_MSS_GetSkills] a  " +
                "  where not exists(select distinct t.TaskId,t.StartTime,t.endtime from EventDetail t where t.TaskId = a.TaskId  " +
                "   and   DATEADD(day,DATEDIFF(day, 0, a.FromDate),CAST(a.FromTime AS DATETIME)) = t.startTime  " +
                "  and   DATEADD(day,DATEDIFF(day, 0, a.ToDate),CAST(a.ToTime AS DATETIME)) = t.EndTime  )   " +
                "  and a.EmployeeID = 360   " +
                "  and DATEADD(day,DATEDIFF(day, 0, a.FromDate),CAST(a.FromTime AS DATETIME)) >='" + startDate + "' " +
                "  and DATEADD(day,DATEDIFF(day, 0, a.FromDate),CAST(a.FromTime AS DATETIME)) <='" + endDate + "' ";
            console.log("query", query);
            dbContext.getQuery(query, parameters, false, function(error, data) {

                console.log("data", data);
                console.log("error", error);
                if (error != null) {
                    return res.sendStatus(401).send({ "message": error })
                }
                if (data != undefined) {
                    if (!data) {
                        return res.json(response([], { "message": [] }));
                    }
                    return res.json(response(data, { "message": data }));
                } else {
                    return res.json(response([], { "message": [] }));
                }
            });

        } else {
            return res.sendStatus(401);
        }
    }


    function getTaskListByDate(req, res) {

        //redo
        console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%getTaskListByDate received body", req.body);
        // console.log("received header", req.header);
        var par = JSON.parse(JSON.stringify(req.body));
        //  var par = jwt.decode(par.token);
        //   console.log("decoded", decoded);
        console.log("par", par);
        console.log("par.startDate", par.startDate);

        var userInfo;
        if (req.token != undefined) {
            console.log("userInfo.EmployeeId1", req.token);
            userInfo = jwt.decode(req.token);
            console.log("user", userInfo);
            console.log("userInfo.EmployeeId2", userInfo);
        } else {
            return res.sendStatus(401).send('missing token');
        }

        console.log("userInfo.EmployeeId", userInfo);



        //type can be: "Day Week Month"
        if (userInfo.EmployeeId != undefined && par.startDate != undefined) {
            // if (!isValidDate(par.startDate)) {
            //     return res.sendStatus(401).send('invalid startDate or endDate');
            // }
            var parameters = [];

            var timearr = (par.startDate).split(" ");

            var start = timearr[0] + " 00:00:000";
            var end = timearr[0] + " 23:59:59";
            console.log(start);
            console.log(end);
            parameters.push({ name: 'EmployeeID', type: TYPES.NVarChar, val: userInfo.EmployeeId });
            parameters.push({ name: 'startDateTime', type: TYPES.Date, start });
            parameters.push({ name: 'endDateTime', type: TYPES.Date, end });


            var query = "select(select " +
                " (select a.TaskName from " + constant.planning + "dbo.View_MSS_GetSkills a where a.TaskId = p.TaskId and  DATEADD(day,DATEDIFF(day, 0, a.FromDate),CAST(a.FromTime AS DATETIME)) = p.StartTime  and DATEADD(day,DATEDIFF(day, 0, a.ToDate),CAST(a.ToTime AS DATETIME)) = p.EndTime  ) as TaskName, p.EventId,p.TaskId, " +
                " Convert(date,p.StartTime) as FromDate,Convert(time,p.StartTime) as FromTime, " +
                " Convert(date,p.EndTime) as ToDate,Convert(time,p.EndTime) as ToTime " +
                ",p.EndTime,p.SupervisorId,p.LocationPostalCode,p.Status from " +
                " EventUserParticipation p " +
                " inner join ManpowerDetail b on b.EventId = p.EventId " +
                "  where b.EmployeeID = @EmployeeID " +
                " and  p.StartTime >=convert(datetime,'" + start + "') " +
                " and p.StartTime <=convert(datetime,'" + end + "')" +
                "   for json path " +
                "  ) as MTSData, " +
                " ( " +
                "  SELECT distinct a.TaskName,a.TaskId,a.FromDate,a.FromTime,a.ToDate,a.ToTime,c.PostalCode,'Pending' as Status " +
                "   FROM " + constant.planning + "[dbo].[View_MSS_GetSkills] a inner join " + constant.planning + "[ManpowerScheduling].[Location] c on a.LocationId = c.id   " +
                //  "a.TaskId not in (select distinct TaskId from EventDetail)  " +
                " where not exists(select distinct t.TaskId,t.StartTime,t.endtime from EventDetail t where t.TaskId = a.TaskId and " +
                "  DATEADD(day,DATEDIFF(day, 0, a.FromDate),CAST(a.FromTime AS DATETIME)) = t.startTime and " +
                "  DATEADD(day,DATEDIFF(day, 0, a.ToDate),CAST(a.ToTime AS DATETIME)) = t.EndTime " +
                " ) " +
                " and a.EmployeeID = @EmployeeID " +
                " and DATEADD(day,DATEDIFF(day, 0, a.FromDate),CAST(a.FromTime AS DATETIME)) >=convert(datetime,'" + start + "') " +
                " and DATEADD(day,DATEDIFF(day, 0, a.FromDate),CAST(a.FromTime AS DATETIME)) <=convert(datetime,'" + end + "') " +
                "  for json path " +
                " ) as MSSData ";
            console.log("getTasksByDate api query", query);

            dbContext.getQuery(query, parameters, false, function(error, data) {
                if (data != undefined && data != null) {
                    console.log("getTaskListByDate data", data);
                    var MTSData = data[0].MTSData;
                    var MSSData = data[0].MSSData;

                    // console.log("MTSData", MTSData);
                    // console.log("MSSData", MSSData);

                    var result;
                    if (MTSData != null && MSSData != null) {
                        console.log("loop1");
                        result = (JSON.parse(MTSData)).concat(JSON.parse(MSSData));
                        console.log("loop1", result);
                    }
                    if (MTSData == null && MSSData == null) {
                        return res.json(response([], { "message": [] }));
                    } else {
                        if (MTSData == null) {
                            result = JSON.parse(MSSData);
                        }
                        if (MSSData == null) {
                            result = JSON.parse(MTSData);
                        }
                    }

                    return res.json(response(data, { "message": result }));
                } else {
                    return res.sendStatus(401);
                }
            });

        } else {
            return res.sendStatus(401);
        }
    }



    function getTaskListByRange(req, res) {

        //redo
        console.log("received body", req.body);
        // console.log("received header", req.header);
        var par = JSON.parse(JSON.stringify(req.body));
        //  var par = jwt.decode(par.token);
        //   console.log("decoded", decoded);
        console.log("par", par);
        console.log("par.LoginID", par.LoginID);

        //type can be: "Day Week Month"
        if (par.TaskId != undefined && par.StartTime != undefined && par.EndTime != undefined) {
            if (isValidDate(par.startDate) || isValidDate(par.endDate)) {
                return res.sendStatus(401).send('invalid startDate or endDate');
            }
            var parameters = [];
            parameters.push({ name: 'EmployeeID', type: TYPES.NVarChar, val: par.EmployeeID });
            parameters.push({ name: 'startDateTime', type: TYPES.Date, val: par.startDate });
            parameters.push({ name: 'endDateTime', type: TYPES.Date, val: par.endDate });


            var query = "select(select " +
                " (select a.TaskName from " + constant.planning + "dbo.View_MSS_GetSkills a where a.TaskId = p.TaskId and  DATEADD(day,DATEDIFF(day, 0, a.FromDate),CAST(a.FromTime AS DATETIME)) = p.StartTime  and DATEADD(day,DATEDIFF(day, 0, a.ToDate),CAST(a.ToTime AS DATETIME)) = p.EndTime  ) as TaskName, p.EventId,p.TaskId, " +
                "p.StartTime,p.EndTime,p.SupervisorId,p.LocationPostalCode,p.Status from " +
                " EventUserParticipation p " +
                " inner join ManpowerDetail b on b.EventId = p.EventId " +
                " inner join EventDetail c on c.EventId = p.EventID " +
                "  where b.EmployeeID = @EmployeeID  " +
                " and @startDateTime >= c.StartTime " +
                " and @endDateTime <= c.EndTime " +
                "   for json path " +
                "  ) as MTSData, " +
                " ( " +
                "  SELECT distinct a.TaskName,a.TaskId,a.FromDate,a.FromTime,a.ToDate,a.ToTime,c.PostalCode,'Pending' as Status " +
                "   FROM " + constant.planning + "[dbo].[View_MSS_GetSkills] a inner join " + constant.planning + "[ManpowerScheduling].[Location] c on a.LocationId = c.id   " +
                " where a.TaskId not in (select distinct TaskId from EventDetail)  " +
                " and a.EmployeeID = @EmployeeID " +
                " and @startDateTime >= DATEADD(day,DATEDIFF(day, 0, a.FromDate),CAST(a.FromTime AS DATETIME)) " +
                " and @endDateTime <= DATEADD(day,DATEDIFF(day, 0, a.ToDate),CAST(a.ToTime AS DATETIME)) " +
                "  for json path " +
                " ) as MSSData ";
            // console.log("query", query);

            dbContext.getQuery(query, parameters, false, function(error, data) {
                if (data != undefined && data != null) {
                    var MTSData = data[0].MTSData;
                    var MSSData = data[0].MSSData;

                    // console.log("MTSData", MTSData);
                    // console.log("MSSData", MSSData);

                    var result;
                    if (MTSData != null && MSSData != null) {
                        console.log("loop1");
                        result = (JSON.parse(MTSData)).concat(JSON.parse(MSSData));
                        console.log("loop1", result);
                    }
                    if (MTSData == null) {
                        result = JSON.parse(MSSData);
                    }
                    if (MSSData == null) {
                        result = JSON.parse(MTSData);
                    }
                    return res.json(response(data, { "message": result }));
                } else {
                    return res.sendStatus(401);
                }
            });

        } else {
            return res.sendStatus(401);
        }
    }
    //helper function  to check valid datetime
    function isValidDate(d) {
        return d instanceof Date && !isNaN(d);
    }
    return {
        getTaskList: getTaskList,
        getTaskListByRange: getTaskListByRange,
        getTaskListRecent: getTaskListRecent,
        getTaskDetail: getTaskDetail,
        getTaskListByDate: getTaskListByDate,
        getWorkingDatesByMonth: getWorkingDatesByMonth
    };
}

module.exports = GetTaskRepository;