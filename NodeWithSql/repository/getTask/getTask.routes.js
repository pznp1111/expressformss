const GetTaskRepository = require('./getTask.respository');
const dbContext = require('../../Database/dbContext');

module.exports = function(router) {
    const getTaskRepository = GetTaskRepository(dbContext);
    router.route('/getTask')
        .post(getTaskRepository.getTaskList);
    router.route('/getTaskDetail')
        .post(getTaskRepository.getTaskDetail);
    router.route('/getTaskListByRange')
        .post(getTaskRepository.getTaskListByRange);
    router.route('/getTaskListRecent')
        .post(getTaskRepository.getTaskListRecent);
    router.route('/getTaskListByDate')
        .post(getTaskRepository.getTaskListByDate);
    router.route('/getWorkingDatesByMonth')
        .post(getTaskRepository.getWorkingDatesByMonth);




}