const express = require('express');

function eRoutes() {
    const router = express.Router();
    var employee = require('./repository/employee/employee.routes')(router);
    var department = require('./repository/department/department.routes')(router);
    var authorization = require('./repository/authorization/authorization.routes')(router);
    var getTask = require('./repository/getTask/getTask.routes')(router);
    var event = require('./repository/Event/event.routes')(router);
    var blockout = require('./repository/blockout/blockout.routes')(router);
    return router;
}

module.exports = eRoutes;